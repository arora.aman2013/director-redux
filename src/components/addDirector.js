import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { addDirector } from '../actions/directorActions'

let director = {
    name: ''
}

class AddDirector extends Component {
    onChange = (e) => {
        director.name = e.target.value
    }
    onSubmit = (e) => {
        e.preventDefault()
        this.props.addDirector(director)
        this.props.history.push('/directors')
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Name</label>
                        <input type = 'text' placeholder = 'Name' name = 'name'/>
                    </div>
                    <button>Add Director</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default connect(null, { addDirector })(withRouter(AddDirector))