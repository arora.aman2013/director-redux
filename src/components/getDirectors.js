import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getAllDirectors, deleteDirectorById } from '../actions/directorActions'
import { withRouter } from 'react-router-dom'
import GetSingleDirector from './getSingleDirector'

class GetDirectors extends Component {
    componentDidMount() {
        this.props.getAllDirectors();
        console.log(this.props.directors)
    }
    onClick = (e) => {
        this.props.history.push(`/directors/add`)
    }
    deleteButton = async (id) => {
        console.log(id)
        await this.props.deleteDirectorById(id)
        this.props.getAllDirectors()
        this.props.history.push('/directors')
    }
    render() {
        return (
            <div>
                <div style = {{display: 'flex', justifyContent: 'center'}}>
                    <button style = {{height: '5vh', width: '10vw'}} onClick = {this.onClick}>Add New director</button>
                </div>
                <div style ={display}>
                    {this.props.directors.map((director => (
                        <GetSingleDirector key = {director.id} director = {director} deleteButton = {this.deleteButton} />
                    )))}
                </div>
            </div>
        )
    }
}

const display = {
    display: 'flex',
    flexWrap: 'wrap'
}

const mapStateToProps = (state) => {
    return{
        directors: state.directors.director
    }
}

export default connect(mapStateToProps, { getAllDirectors, deleteDirectorById })(withRouter(GetDirectors))
