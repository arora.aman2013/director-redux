import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { deleteDirectorById, getAllDirectors } from '../actions/directorActions'

class getSingleDirector extends Component {
    editButton = (e) => {
        this.props.history.push(`/directors/edit/${this.props.director.id}`)
    }
    // deleteButton = (id) => {
    //     console.log(id)
    //     let director = this.props.deleteDirectorById(id)
    //     console.log(director)
    // }
    render() {
        return (
            <div style = {directorStyle}>
                <h3 style = {{marginBottom: '1%'}}>{this.props.director.name}</h3>
                <div style = {displayFlex}>
                    <button style = {buttonStyle} onClick = {this.editButton}>Edit</button>
                    <button style = {buttonStyle} onClick = {this.props.deleteButton.bind(this, this.props.director.id)}>Delete</button>
                </div>
            </div>
        )
    }
}

const displayFlex = {
    marginTop: '2%',
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
}

const buttonStyle = {
    height: '5vh',
    width: '5vw',
    cursor: 'pointer'
}
const directorStyle = {
    width: '40%',
    margin: '2.5%',
    backgroundColor: 'grey',
    padding: '2%',
}
// export default (null, { deleteDirectorById })(withRouter(getSingleDirector))
export default connect(null, { deleteDirectorById, getAllDirectors })(withRouter(getSingleDirector))
