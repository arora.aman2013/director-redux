import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { editDirectorById } from '../actions/directorActions'

let director = {
    name: ''
}

class Adddirector extends Component {
    state = {
        director: []
    }
    componentDidMount() {
        
    }
    onChange = (e) => {
        // console.log(e.target.value)
        director.name = e.target.value
    }
    onSubmit = (e) => {
        e.preventDefault()
        editDirectorById(director, this.props.params.id)
    }
    render() {
        return (
            <div style = {divStyle}>
                <form style = {formStyle} onChange = {this.onChange} onSubmit = {this.onSubmit}>
                    <div style = {labelInputStyle}>
                        <label>Name</label>
                        <input type = 'text' placeholder = 'Name' name = 'name' />
                    </div>
                    <button>Add director</button>
                </form>
            </div>
        )
    }
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

const formStyle = {
    width: '30%',
    backgroundColor: 'grey',
    padding: '2%'
}

const labelInputStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '1%'
}

export default connect(null, { editDirectorById })(withRouter(Adddirector))
