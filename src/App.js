import React, { Component } from 'react'
import './App.css';
import { Provider } from 'react-redux';
import store from './store'
import GetDirector from './components/getDirectors'
import AddDirector from './components/addDirector'
import EditDirectorById from './components/editDirectorById'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Provider store = {store} >
        <Router>
          <Switch>
          <Route path = '/directors/add' component = {AddDirector}/>
          <Route path = '/directors/edit/:id' render = {(props) => (
              <EditDirectorById params = {props.match.params} />
            )} />
          <Route path = '/directors' component = {GetDirector} />
        </Switch>
        </Router>
      </Provider>
    )
  }
}

export default App;
