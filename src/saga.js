import { put, takeLatest, all, fork } from 'redux-saga/effects'

function* getAllDirectors(){
    const json = yield fetch('http://localhost:8000/api/directors')
    .then(res => res.json())
    // console.log('saga')
    yield put({type: 'fetchedData', json: json})
} 

function* actionWatcher(){
    yield takeLatest('getAll', getAllDirectors)
}

function* addDirector(director){
    // console.log(JSON.stringify(director))
    const json = yield fetch('http://localhost:8000/api/directors/', {
            method:'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: JSON.stringify(director.data)
        })
        .then((res) =>{
            if(res.ok)
                return res.json()       
            })
        .then((datajson ) => {
            console.log(datajson);
        });
}

function* actionWatcher1(){
    yield takeLatest('add', addDirector)
}

function* deleteDirector(id){
    console.log(id.data)
    const json = yield fetch(('http://localhost:8000/api/directors/'+id.data), {
        method: 'delete'
    }).then(res => res.json())
}

function* actionWatcher2(){
    yield takeLatest('delete', deleteDirector)
}

function* editDirector(director){
    console.log('saga')
    const json = yield fetch(('http://localhost:8000/api/directors/'+director.id), {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            },
            mode: 'cors', 
            body: JSON.stringify(director.data)
    }).then(res => {
        if(res.ok)
            res.json()
        })
    // yield put({type: 'editDirector', json: json})
}

function* actionWatcher3(){
    console.log('Action Watcher')
    yield takeLatest("editById", editDirector)
}

export default function* rootSaga(){
    yield all([
        fork(actionWatcher),
        fork(actionWatcher1),
        fork(actionWatcher2),
        fork(actionWatcher3)
    ])
}