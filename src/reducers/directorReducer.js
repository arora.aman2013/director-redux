const initialState = {
    director: [],
}

export default function(state = initialState, action){
    switch(action.type) {
        case 'fetchedData':
            return {
                ...state,
                director: action.json
            }
        // case 'editDirector':
        //     return {
        //         ...state,
        //         director: action.json
        //     }
        default: return state;
    }
}
