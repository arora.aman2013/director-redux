export const getAllDirectors = () => (
    {
    type: 'getAll'
    }
)

export const addDirector = (data) => (
    // console.log('director add'),
    {
        type: 'add',
        data
    }
)

export const deleteDirectorById = (data) => (
    // console.log('delete Director'),
    {
        type: 'delete',
        data
    }
)

// export const getDirectorById = (id) => (
//     // console.log('delete Director'),
//     {
//         type: 'getOne',
//         id
//     }
// )

export const editDirectorById = (data, id) => (
    console.log('edit Director'),
    {
        type: 'editById',
        data,
        id
    }
)